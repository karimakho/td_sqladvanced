-- Script DDL
-- Creation des tables person,visit et place:
CREATE TABLE Person (
	person_id INT ,
    person_name VARCHAR(50),
    health_status VARCHAR(50),
    confirmed_status_date TIMESTAMP,
    primary key(person_id)
);
CREATE TABLE Visit (
	visit_id INT ,
    person_id INT,
    place_id INT,
    start_datetime TIMESTAMP,
    end_datetime TIMESTAMP,
    primary key(visit_id),
    foreign key(place_id) references place (place_id),
    foreign key(person_id) references person(person_id)
);
CREATE TABLE  Place ( 
    place_id INT ,
    place_name VARCHAR(50),
    place_type VARCHAR(50),
    primary key(place_id)
);

DROP TABLE if exists visit
;
DROP TABLE if exists place
;
DROP TABLE if exists person
;
