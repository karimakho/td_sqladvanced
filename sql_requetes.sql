-- Noms + statuts santé des personnes que Landyn Greer a croisé (même lieu, même moment)
-- 1) Lieux fréquentés par Landyn Greer:
SELECT v.place_id, pl.place_name
FROM visit AS v 
INNER JOIN place AS pl
ON v.place_id = pl.place_id 
WHERE person_id = 1;
-- Result: Place nr 33 and Place nr 88
-- 2) Noms des personnes ayant fréquenté le lieu 33 et leur statut
SELECT p.person_name, p.health_status
FROM person AS p 
INNER JOIN visit AS v 
ON p.person_id = v.person_id
WHERE place_id = 33 
ORDER BY p.person_id;
-- 3) Noms des personnes ayant fréquenté le lieu 88 et leur statut
SELECT p.person_name, p.health_status
FROM person AS p 
INNER JOIN visit AS v 
ON p.person_id = v.person_id
WHERE place_id = 88 
ORDER BY p.person_id;
-- 4) Noms des personnes que Landyn Greer a fréquenté dans le lieu 33 et 88 et leur statut
SELECT p.person_name AS "LG_seen_people", p.health_status AS "status"
FROM person p INNER JOIN visit v 
ON p.person_id = v.person_id
WHERE tsrange(start_datetime, end_datetime) &&
	  tsrange '(2020-10-07 20:54:00.000,2020-10-08 03:44:00.000)'
AND p.person_name != 'Landyn Greer'
AND v.place_id IN (33,88)

-- Nombre de malades croisés dans un bar au même moment
-- 1) Création d'une nouvelle table avec jointure des 3 tables visit, person, place
CREATE TABLE jointab
	(VISIT_ID				 INT      				 NULL,
	 PERSON_ID               INT      				 NULL,
	 PLACE_ID                INT     				 NULL,
	 START_DATETIME          TIMESTAMP 	 	 		 NULL,
     END_DATETIME            TIMESTAMP 	 			 null,
     PERSON_NAME             VARCHAR(30) 	      	 NULL,
     HEALTH_STATUS           VARCHAR(10) 	      	 NULL,
     CONFIRMED_STATUS_DATE   TIMESTAMP 		  		 NUll,
     PLACE_NAME				 VARCHAR(30)			 null,
     PLACE_TYPE              VARCHAR(30)        	 null
     );

-- 2) Insertion des données nouvelle table
INSERT INTO jointab 
	SELECT v.VISIT_ID, v.PERSON_ID, v.PLACE_ID, v.START_DATETIME, v.END_DATETIME,
	p.PERSON_NAME, p.HEALTH_STATUS , p.CONFIRMED_STATUS_DATE, pl.place_name, pl.PLACE_TYPE 
	FROM visit v
	INNER JOIN person p ON v.person_id = p.person_id
	INNER JOIN place pl ON v.place_id  = pl.place_id;
  -- 3) Jointure de table  
CREATE TABLE sick_ppl_bar  
	SELECT * FROM jointab
	WHERE health_status = 'Sick'
	AND place_type = 'Bar'
	AND confirmed_status_date <= start_datetime;
--  4) Personnes ayant fréquenté le bar toute période confondue
SELECT place_name as "Bar", count(person_id) as "Nb de personnes"
FROM sick_bar 
GROUP BY place_name;
-- 5) Personnes ayant fréquenté le bar en même temps
SELECT t1.place_name AS "bars_name", COUNT (t2.person_id) AS "sick_people"
FROM sick_bar t1, sick_bar t2
WHERE t2.place_id = t1.place_id
AND t1.start_datetime <= t2.end_datetime
AND t1.end_datetime >= t2.start_datetime
AND t1.person_name != t2.person_name 
GROUP BY t1.place_name;

-- Noms des personnes que Taylor Luna a croisé :
-- 1)
CREATE VIEW Taylor_Luna AS
SELECT visit_id,person_id,place_id,start_datetime,end_datetime 
FROM visit
WHERE person_id = 4; 
-- 2)
SELECT person_name, health_status,start_datetime,end_datetime, confirmed_status_date, pl.place_id
INTO Taylorluna
FROM person AS p
INNER JOIN (place AS pl
    INNER JOIN visit AS v
    on pl.place_id = v.place_id )
on v.person_id = p.person_id ;
-- 3)
SELECT t1.person_name, t1.health_status, t1.place_id 
INTO Taylorluna_seen_ppl
FROM Taylorluna AS t1 
INNER JOIN Taylorluna AS t2
ON t1.start_datetime <=  t2.end_datetime
AND t1.end_datetime >= t2.start_datetime
WHERE t1.person_name = 'Taylor Luna' 
AND t1.person_name != t2.person_name  
AND t1.place_id = t2.place_id;
-- On voit d'après le tableau qu'il a croisé 12 personnes 

-- Nombre de malades par endroit (place_name) + durée moyenne de leurs visites dans cet endroit
-- 1) Création d'un intervalle de temps de visite par ajout sur jointab
ALTER TABLE jointab 
ADD COLUMN duree_interval BINARY;
UPDATE jointab 
SET duree = age(end_datetime, start_datetime); -- Calcul d'intervalle

-- 2) Nombre de malades groupé par endroit
SELECT place_name AS "place", count(person_id) AS "count_sick_ppl", 
	   AVG(duree)::interval(0) AS "average_timestamp" -- Moyenne de temps arrondie 
FROM jointab 
WHERE health_status = 'Sick'
GROUP BY place_name

-- Nombre de sains (non malades) par endroit (place_name) + durée moyenne de leurs visites dans cet endroit
SELECT place_name AS"place", count(person_id) AS "count_healthy_ppl", 
	   AVG(duree)::interval(0) AS "average_timestamp_visit"
FROM jointab 
WHERE health_status != 'Sick'
GROUP BY place_name
